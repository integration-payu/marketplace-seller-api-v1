<?

function addSign($data) {

	$secretKey = 'secret';

        ksort($data);
        $signString = "";
        foreach($data as $one){
            $signString .=  $one;    
        }
        $data['timestamp'] = time();
        $data['signature'] = hash_hmac("sha256", $signString . $data['timestamp'], $secretKey);
        return $data; 
}

function sendRequest($data, $url) {
        
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);

        return curl_exec($ch);

        $curlerrcode = curl_errno($ch);
        $curlerr = curl_error($ch);

        if($curlerrcode || $curlerr) die("Curl Err Code: $curlerrcode, Curl Err: $curlerr");
     
}



$data = array(
	"merchant" => "merchant",
	"sellerType" => "INDIVIDUAL",
//	"automaticEmail" => "automatic@example.com",
	"city" => "Moscow",
	"companyName" => "Test Company",
	"countryCode" => "ru",
	"email" => "fb00@bk.ru",
	"fax" => "9067770000",
	"firstName" => "John",
	"lastName" => "Doe",
	"fiscalCode" => "TST999888",
	"homepage" => "www.payu.ru",
	"kycStatus" => "PENDING",
	"phone" => "9067770001",
	"registrationNumber" => "111111",
	"state" => "Test State",
	"street" => "Test Street",
	"techEmail" => "fb00@bk.ru",
	"techPhone" => "9067770002",
	"techWeb" => "www.payu.ru",
	"address" => "ул. Пушкина, д. 23а",
	"zip" => "117321"
);



// Создание продавца
$url = 'https://secure.payu.ru/api/marketplace/v1/seller';

$result = json_decode(sendRequest(addSign($data), $url), 1);

$sellerCode = $result['sellerCode'];	// сохранить



// Получение информации о продавце
$data = array(
	"merchant" => "merchant",
);

$result = json_decode(file_get_contents($url . '?' . http_build_query(addSign($data))), 1);



// Обновление информации о продавце
$url = 'https://secure.payu.ru/api/marketplace/v1/seller/' . $sellerCode;

$result = json_decode(sendRequest(addSign($data), $url), 1);



// Вариант обработки результата
if ($result['meta']['response']['httpCode'] != 200) die('Error: '.$result['meta']['response']['httpMessage']);

print_r($result);



